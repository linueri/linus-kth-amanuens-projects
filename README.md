# Linus KTH amanuens projects

update 3: Finally back. Have uploaded the cooling capacity file with some calculations inside. Think I have kinda recreated the cooling capacities from the polynomials – will start reflecting upon its meaning next session.

Update 2:
Looking into how to register a Julia package. AFAIK the GSHP package passes most of the remarks found on https://julialang.org/contribute/developing_package/ with the exception of using Gitlab instead of Github. Will look farther into and test it out with my own creation I think.

Update 1:

Currently having issues since the textfile is converted to cell-array format which limits logical commands. Trying to find a way to separate and plot individual measurements.

Summary from looking at the different files is that Anders De la Rose 2017-2018 and 2019 is the most plentiful in data, the other two giving a lot of data over a very short period.

Parameters in "Anders De la Rose_Atlas":
1. in.components.pump.brine.rpm_feedback
2. in.sensors.temp.brine_in
3. in.sensors.temp.brine_out
4. in.sensors.temp.condenser_in
5. in.sensors.temp.condenser_out
6. in.sensors.temp.discharge_pipe
7. out.components.pump.brine.rpm_gui
8. out.components.pump.condenser.rpm_gui
9. out_log.out_log_components.compressor.actual_compressor_rpm
10. out_log.out_log_sensors.cop_calc.heat.q_heat_kw
11. out_log.out_log_sensors.cop_calc.tap_water.q_heat_kw
12. out_log.out_log_sensors.cop_calc.total_consumed_power_kw
13. out_log.out_log_sensors.temp_dew_pressure_high
14. out_log.out_log_sensors.temp_dew_pressure_low

23530455 time-stamps, from 11:09:36 to 14:05:39 on the 19th of Jan 2022

Parameters in "Anders De la Rose_Diplomat Inveter L_2017 2018_Old":
1. in.components.pump.brine.rpm_feedback
2. in.sensors.temp.brine_in
3. in.sensors.temp.brine_out
4. in.sensors.temp.condenser_in
5. in.sensors.temp.condenser_out
6. in.sensors.temp.discharge_pipe
7. out.components.pump.brine.rpm_gui
8. out.components.pump.condenser.rpm_gui
9. out_log.out_log_components.compressor.actual_compressor_rpm
10. out_log.out_log_sensors.temp_dew_pressure_high
11. out_log.out_log_sensors.temp_dew_pressure_low

from 2017-10-17_15:27:13 to 2018-10-17_04:02:41 with 20473023 time-stamps

Parameters in "Anders De la Rose_Diplomat Inveter L_2019 tom OKT 07":
1. in.components.pump.brine.rpm_feedback
2. in.sensors.temp.brine_in
3. in.sensors.temp.brine_out
4. in.sensors.temp.condenser_in
5. in.sensors.temp.condenser_out
6. in.sensors.temp.discharge_pipe
7. out.components.pump.brine.rpm_gui
8. out.components.pump.condenser.rpm_gui
9. out_log.out_log_components.compressor.actual_compressor_rpm
10. out_log.out_log_sensors.temp_dew_pressure_high
11. out_log.out_log_sensors.temp_dew_pressure_low

'2019-01-19_09:38:21' to '2019-10-07_07:32:39' with 5941588 time-stamps

Parameters in "Martin Hamberg":
1. in.components.pump.brine.rpm_feedback
2. in.sensors.temp.brine_in
3. in.sensors.temp.brine_out
4. in.sensors.temp.condenser_in
5. in.sensors.temp.condenser_out
6. in.sensors.temp.discharge_pipe
7. out.components.pump.brine.rpm_gui
8. out.components.pump.condenser.rpm_gui
9. out_log.out_log_components.compressor.actual_compressor_rpm
10. out_log.out_log_sensors.temp_dew_pressure_high
11. out_log.out_log_sensors.temp_dew_pressure_low

'2021-05-18_00:09:59' to '2021-05-18_00:31:52' with 832 time-stamps
